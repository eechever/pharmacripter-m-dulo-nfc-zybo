 /**
 * @file    mio.c
 * @brief   Fichero que implementa las funciones relacionadas con
 * 			los pines MIO de la Zybo Zynq-7000
 * @author  Eneko Echeverria
 * @date    09/04/2016
 * @version	1.0
 */

#include <stdio.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xscugic.h"
#include "xil_exception.h"
#include "xgpiops.h"

#include "mio.h"
XGpioPs Gpio;

/**
* @brief	Funci�n que inicializa los pines MIO
* @return	[void]
*/
void MIO_init()
{
	XGpioPs_Config *ConfigPtr;
	int Status;
	ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	Status = XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);
}

/**
* @brief	Funci�n que lee el valor de un pin MIO
* @param	[int] pin -  n�mero de pin
* @return	[int] - valor del pin
*/
int MIO_read (int pin)
{
	int value;
	XGpioPs_SetDirectionPin(&Gpio,pin,0);
	value=XGpioPs_ReadPin(&Gpio,pin);
	return value;
}

/**
* @brief	Funci�n que establece el valor de un pin MIO
* @param	[int] pin -  n�mero de pin
* @param	[int] value - valor del pin
* @return	[void]
*/
void MIO_write(int pin, int value)
{
	XGpioPs_SetDirectionPin(&Gpio,pin,1);
	XGpioPs_SetOutputEnablePin(&Gpio,pin,1);
	XGpioPs_WritePin(&Gpio,pin,value);
}
