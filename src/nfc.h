 /**
 * @file    nfc.h
 * @brief   Fichero de cabecera de las funciones NFC
 * @author  Eneko Echeverria
 * @date    19/05/2016
 * @version	1.0
 */

#ifndef NFC_H_
#define NFC_H_

unsigned int NFC_initialize(); //Initializes the NFC board and returns the firmware version
unsigned int NFC_write_identifier(unsigned char *name, unsigned char *password);
unsigned int NFC_read_identifier(unsigned char *name, unsigned char *password);
unsigned int NFC_write_alarm(unsigned char dir, unsigned char *name, unsigned char qty, unsigned char *unit, unsigned char *time);
unsigned int NFC_DEBUG_read_alarm(unsigned char dir);
unsigned int NFC_format_card();
unsigned char NFC_restore_card();
#endif /* NFC_H_ */
