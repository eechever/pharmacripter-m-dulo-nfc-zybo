 /**
 * @file    PN532.h
 * @brief   Fichero de cabecera de las funciones del m�dulo PN532
 * @author  Eneko Echeverria
 * @date    09/04/2016
 * @version	1.0
 */

#ifndef PN532_H_
#define PN532_H_

#include "stdint.h"

#define PN532_PREAMBLE 0x00
#define PN532_STARTCODE1 0x00
#define PN532_STARTCODE2 0xFF
#define PN532_POSTAMBLE 0x00

#define PN532_HOSTTOPN532 0xD4

#define PN532_FIRMWAREVERSION 0x02
#define PN532_GETGENERALSTATUS 0x04
#define PN532_SAMCONFIGURATION  0x14
#define PN532_INLISTPASSIVETARGET 0x4A
#define PN532_INDATAEXCHANGE 0x40
#define PN532_MIFARE_READ 0x30
#define PN532_MIFARE_WRITE 0xA0

#define PN532_AUTH_WITH_KEYA 0x60
#define PN532_AUTH_WITH_KEYB 0x61


#define PN532_WAKEUP 0x55

#define  PN532_SPI_STATREAD 0x02
#define  PN532_SPI_DATAWRITE 0x01
#define  PN532_SPI_DATAREAD 0x03
#define  PN532_SPI_READY 0x01

#define PN532_MIFARE_ISO14443A 0x0

#define KEY_A	1
#define KEY_B	2


void PN532_begin();
unsigned int PN532_get_firmware_version();
unsigned int PN532_sam_config();
uint32_t PN532_read_passive_target_id(unsigned char cardbaudrate);
unsigned int PN532_authenticate_block(unsigned char cardnumber /*1 or 2*/,uint32_t cid /*Card NUID*/, unsigned char blockaddress /*0 to 63*/,unsigned char authtype/*Either KEY_A or KEY_B */, unsigned char * keys);
unsigned int PN532_read_memory_block(unsigned char cardnumber /*1 or 2*/,unsigned char blockaddress /*0 to 63*/, unsigned char * block);
unsigned int PN532_write_memory_block(unsigned char cardnumber /*1 or 2*/,unsigned char blockaddress /*0 to 63*/, unsigned char * block);

#endif /* PN532_H_ */
