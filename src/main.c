
 /**
 * @file    main.c
 * @brief   Fichero principal
 * @author  Eneko Echeverria
 * @date    09/04/2016
 * @version	1.0
 */

#include "xil_printf.h"
#include "PN532.h"
#include "nfc.h"

#define BLOCK_LENGTH	16

int main()
{

	unsigned char a[BLOCK_LENGTH]={'M','y','o','l','a','s','t','a','n',' ',' ',' ',' ',' ',' ',' '};//{0xF0,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F};
	unsigned char k[BLOCK_LENGTH]={'D','a','l','s','y',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '};
	unsigned char b[BLOCK_LENGTH]={'m','g',0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F};
	unsigned char t[]={0x00,0x60};
	unsigned char s[]={0x00,0x2F};
	unsigned char c[BLOCK_LENGTH];
	unsigned char d[BLOCK_LENGTH];
	int i;

	NFC_initialize();
	NFC_format_card();
	//NFC_restore_card();
	//NFC_write_identifier(a,b);
	//NFC_read_identifier(c,d);

	xil_printf("\n***USER:  ");
	for(i=0; i<BLOCK_LENGTH; i++)
	{
		xil_printf("%c",*(c+i));
		//xil_printf(" %02x",*(a+i));
	}

	xil_printf("\n***PASSWORD: ");
	for(i=0; i<BLOCK_LENGTH; i++)
	{
		//xil_printf(" %c",*(b+i));
		xil_printf(" %02x",*(d+i));
	}

	NFC_write_alarm(12,a,7,b,s);
	NFC_write_alarm(13,k,7,b,t);
	//NFC_write_alarm(28,a,7,b,b);
	//NFC_DEBUG_read_alarm(0x0C);


	return 0;
}
