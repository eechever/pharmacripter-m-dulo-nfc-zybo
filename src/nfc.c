 /**
 * @file    nfc.c
 * @brief   Fichero que implementa las funciones del
 * 			servicio NFC a nivel de aplicaci�n para el
 * 			proyecto Pharmacripter
 * @author  Eneko Echeverria
 * @date    19/05/2016
 * @version	1.0
 */

#include "PN532.h"
#include "xil_printf.h"
#include "nfc.h"

#define	SECTOR_LENGTH		4		//N�mero de bloques de cada sector
#define BLOCK_LENGTH		16		//Longitud en bytes de cada bloque


#define SECTOR_USER			2		//Sector destinado a la informaci�n del usuario
#define	SECTOR_ALARM		3		//Primer sector destinado al almacenamiento de alarmas
#define NUM_SECTOR_ALARM	3		//N�mero de sectores destinados al almacenamiento de las alarmas

#define	BLOCK_USER			0		//Bloque destinado a guardar el usuario
#define BLOCK_PASS			1		//Bloque destinado a guardar la contrase�a


unsigned char cardbaudrate=PN532_MIFARE_ISO14443A;

unsigned char default_key[] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
unsigned char pharma_key_A[] = {0x70,0x68,0x61,0x72,0x6D,0x61};
unsigned char pharma_key_B[] = {0xF0,0x67,0x61,0x6C,0x75,0x72};

unsigned int NFC_change_trailer(unsigned char sector, unsigned char key_type, unsigned char *key, unsigned char *trailer);

/**
* @brief	Funci�n que inicializa el servicio NFC
* @return	[unsigned int] - Confirmaci�n del proceso de inicializaci�n
*/
unsigned int NFC_initialize()
{
	unsigned int version;

	PN532_begin();
	version=PN532_get_firmware_version();
	if(!version)
	{
		xil_printf("\nDidn't find PN53x board");
		return FALSE;
	}
	xil_printf("\n***Found chip PN5 | %02x",(version>>24) & 0xFF);
	xil_printf("\n***Firmware version | %d.%d",(version>>16) & 0xFF, (version>>8) & 0xFF);
	xil_printf("\n***Supports | %02x",version & 0xFF);

	// configure board to read RFID tags and cards
	if(!PN532_sam_config()){
		xil_printf("\nSAM config failed");
		return FALSE;
	}

	return TRUE;
}


/**
* @brief	Funci�n que escribe la informaci�n sobre el usuario
* 			en la tarjeta
* @param	[unsigned char *] name - Nombre
* @param	[unsigned char *] password - Contrase�a
* @return	[unsigned int] - Confirmaci�n del proceso de escritura
*/
unsigned int NFC_write_identifier(unsigned char *name, unsigned char *password)
{
	uint32_t id=0;
	unsigned char address;

	id=PN532_read_passive_target_id(cardbaudrate);

	address=SECTOR_USER*SECTOR_LENGTH+BLOCK_USER;
	if(!PN532_authenticate_block(1,id,address,KEY_B,pharma_key_B)) return FALSE;

	if(!PN532_write_memory_block(1,address,name)) return FALSE;

	address=SECTOR_USER*SECTOR_LENGTH+BLOCK_PASS;
	if(!PN532_write_memory_block(1,address,password)) return FALSE;

	return TRUE;
}

/**
* @brief	Funci�n que lee la informaci�n sobre el usuario
* 			de la tarjeta
* @param	[unsigned char *] name - Direcci�n de memoria
* 			donde se va a guardar el nombre leido
* @param	[unsigned char *] password - Direcci�n de memoria
* 			donde se va a guardar la contrase�a leida
* @return	[unsigned int] - Confirmaci�n del proceso de lectura
*/
unsigned int NFC_read_identifier(unsigned char *name, unsigned char *password)
{
	uint32_t id=0;
	unsigned char address;

	id=PN532_read_passive_target_id(cardbaudrate);

	address=SECTOR_USER*SECTOR_LENGTH+BLOCK_USER;
	if(!PN532_authenticate_block(1,id,address,KEY_B,pharma_key_B))
	{
		xil_printf("\n------------------auth failed");
		return FALSE;
	}

	if(!PN532_read_memory_block(1,address,name)) return FALSE;

	address=SECTOR_USER*SECTOR_LENGTH+BLOCK_PASS;
	if(!PN532_read_memory_block(1,address,password)) return FALSE;

	return TRUE;
}

/**
* @brief	Funci�n que escribe una alarma en la memoria
* @param	[unsigned char] dir - Bloque de memoria donde
* 			se quiere guardar la alarma
* @param	[unsigned char *] name - Nombre del f�rmaco
* @param	[unsigned char] qty - Cantidad
* @param	[unsigned char *] unit - Unidades de cantidad
* @param	[unsigned char *] time - Tiempo restante en minutos
* @return	[unsigned int] - Confirmaci�n del proceso de escritura
*/
unsigned int NFC_write_alarm(unsigned char dir, unsigned char *name, unsigned char qty, unsigned char *unit, unsigned char *time)
{
	uint32_t id=0;
	unsigned char i;
	unsigned char data[BLOCK_LENGTH];

	if((dir+1)%4==0 || (dir<SECTOR_ALARM*SECTOR_LENGTH || dir>=(SECTOR_ALARM+NUM_SECTOR_ALARM)*SECTOR_LENGTH))
	{
		xil_printf("-----------------------------invalid direction");
		return FALSE;
	}


	for(i=0; i<10; i++)
	{
		*(data+i)=*(name+i);
	}
	*(data+10)=qty;
	for(i=11; i<14; i++)
	{
		*(data+i)=*(unit+i-11);
	}
	*(data+14)=*(time+0);
	*(data+15)=*(time+1);

	id=PN532_read_passive_target_id(cardbaudrate);
	if(!PN532_authenticate_block(1,id,dir,KEY_B,pharma_key_B)) return FALSE;
	if(!PN532_write_memory_block(1,dir,data)) return FALSE;

	return TRUE;
}

/**
* @brief	Funci�n de testeo que lee una alarma de la memoria
* @param	[unsigned char] dir - Bloque de memoria desde donde
* 			se quiere leer la alarma
* @return	[unsigned int] - Confirmaci�n del proceso de lectura
*/
unsigned int NFC_DEBUG_read_alarm(unsigned char dir)
{
	uint32_t id=0;
	unsigned char data[BLOCK_LENGTH];

	id=PN532_read_passive_target_id(cardbaudrate);
	if(!PN532_authenticate_block(1,id,dir,KEY_B,pharma_key_B)) return FALSE;
	if(!PN532_read_memory_block(1,dir,data)) return FALSE;

	return TRUE;
}

/**
* @brief	Funci�n que configura una tarjeta de memoria virgen con
* 			todos los par�metros por defecto para que pueda ser usado
* 			dentro del sistema Pharmacripter.
* @return	[unsigned int] - Confirmaci�n del proceso de formateo
*/
unsigned int NFC_format_card()
{
	unsigned char access_bits_id[] = {0x0F, 0x00, 0xFF, 0x69};
	unsigned char access_bits_al[] = {0x7F, 0x07, 0x88, 0x69};

	unsigned char trailer_id[BLOCK_LENGTH];
	unsigned char trailer_al[BLOCK_LENGTH];

	int i;

	for(i=0; i<6; i++)
	{
		*(trailer_id+i)=*(pharma_key_A+i);
		*(trailer_al+i)=*(pharma_key_A+i);
	}

	for(i=6; i<10; i++)
	{
		*(trailer_id+i)=*(access_bits_id+i-6);
		*(trailer_al+i)=*(access_bits_al+i-6);
	}

	for(i=10; i<16; i++)
	{
		*(trailer_id+i)=*(pharma_key_B+i-10);
		*(trailer_al+i)=*(pharma_key_B+i-10);
	}

	NFC_change_trailer((unsigned char) SECTOR_USER, KEY_A, default_key, trailer_id);
	for(i=0; i<NUM_SECTOR_ALARM; i++)
	{
		NFC_change_trailer((unsigned char) SECTOR_ALARM+i, KEY_A, default_key, trailer_al);
	}

	return TRUE;
}

/**
* @brief	Funci�n que restablece los valores por defecto de una
* 			tarjeta que previamente haya sido configurada para usarla
* 			dentro del sistema Pharmacripter
* @return	[unsigned char] - Confirmaci�n del proceso de formateo
*/
unsigned char NFC_restore_card()
{
	unsigned char access_bits[] = {0xFF, 0x07, 0x80, 0x69};
	unsigned char trailer[BLOCK_LENGTH];

	int i;

	for(i=0; i<6; i++)
	{
		*(trailer+i)=*(default_key+i);
	}

	for(i=6; i<10; i++)
	{
		*(trailer+i)=*(access_bits+i-6);
	}

	for(i=10; i<16; i++)
	{
		*(trailer+i)=*(default_key+i-10);
	}

	NFC_change_trailer((unsigned char) SECTOR_USER, KEY_B, pharma_key_B, trailer);
	for(i=0; i<NUM_SECTOR_ALARM; i++)
	{
		NFC_change_trailer((unsigned char) SECTOR_ALARM+i, KEY_B, pharma_key_B, trailer);
	}

	return TRUE;
}

/**
* @brief	Funci�n que configura el trailer de un sector
* @param	[unsigned char] sector - Sector cuyo trailer se quiere modificar
* @param	[unsigned char] key_type - Tipo de clave que se va a usar para
* 			autenticarse en el sector
* @param	[unsigned char *] key - Clave de autenticaci�n
* @param	[unsigned char *] trailer - Secuencia de bytes del nuevo trailer
* @return	[unsigned int] - Confirmaci�n del proceso de escritura
*/
unsigned int NFC_change_trailer(unsigned char sector, unsigned char key_type, unsigned char *key, unsigned char *trailer)
{
	uint32_t id=0;
	unsigned char address;

	address=sector*SECTOR_LENGTH+3;
	id=PN532_read_passive_target_id(cardbaudrate);
	if(!PN532_authenticate_block(1,id,address,key_type,key)) return FALSE;
	if(!PN532_write_memory_block(1,address,trailer)) return FALSE;
	xil_printf("\nTRAILER CHANGED!");
	return TRUE;

}
