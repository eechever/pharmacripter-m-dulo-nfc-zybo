 /**
 * @file    mio.c
 * @brief   Cabecera de los servicios relacionados con los MIO
 * @author  Eneko Echeverria
 * @date    09/04/2016
 * @version	1.0
 */

#ifndef MIO_H_
#define MIO_H_

void MIO_init();
int MIO_read (int pin);
void MIO_write(int pin, int value);

#endif /* MIO_H_ */
