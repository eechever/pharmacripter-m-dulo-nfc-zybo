 /**
 * @file    PN532.c
 * @brief   Fichero que implementa las funciones de control
 * 			del m�dulo de comunicaciones NFC PN532
 * @author  Eneko Echeverria
 * @date    09/04/2016
 * @version	1.0
 */

#include <stdio.h>
#include <string.h>
#include "mio.h"
#include "spi.h"
#include "sleep.h"
#include "PN532.h"

#include "xil_printf.h"

#define TRUE 1
#define FALSE 0
#define PN532_PACKBUFFSIZ 64

unsigned char pn532response_firmwarevers[] = {0x00, 0xFF, 0x06, 0xFA, 0xD5, 0x03};
unsigned char pn532_packetbuffer[PN532_PACKBUFFSIZ];

int PN532_send_command_check_ack(unsigned char *cmd, int cmdlen, int timeout);

/**
* @brief	Funci�n que inicializa el perif�rico
* @return	[void]
*/
void PN532_begin()
{
	MIO_init();
	MIO_write(SEL,LOW);
	sleep(1);
	pn532_packetbuffer[0]=PN532_FIRMWAREVERSION;
	PN532_send_command_check_ack(pn532_packetbuffer,1,0);
}

/**
* @brief	Funci�n que lee el firmware del m�dulo de comunicaciones
* @return	[unsigned int] - Versi�n del firmware
*/
unsigned int PN532_get_firmware_version()
{
	unsigned int response;
	pn532_packetbuffer[0] = PN532_FIRMWAREVERSION;
	if(!PN532_send_command_check_ack(pn532_packetbuffer,1,0)) return 0;
	SPI_read_data(pn532_packetbuffer,12);
	if(0!=strncmp((char *)pn532_packetbuffer, (char *)pn532response_firmwarevers, 6)) return 0;

	response = pn532_packetbuffer[6+1];
	response <<= 8;
	response |= pn532_packetbuffer[7+1];
	response <<= 8;
	response |= pn532_packetbuffer[8+1];
	response <<= 8;
	response |= pn532_packetbuffer[9+1];

	return response;
}

/**
* @brief	Funci�n que env�a un comando al m�dulo de comunicaciones
* 			y comprueba que �ste responde con un ACK
* @param	[unsigned char *] cmd - Puntero a la direcci�n de memoria
* 			donde se encuentra la informaci�n sobre el comando a enviar
* @param	[int] cmdlen - Longitud del comando
* @param	[int] timeout - Valor del timeout. Si dicho valor es igual
* 			a cero, no se tiene en cuenta el timeout
* @return	[int] - Confirmaci�n de la recepci�n del ACK
*/
int PN532_send_command_check_ack(unsigned char *cmd, int cmdlen, int timeout)
{
	int timer=0;
	SPI_write_command(cmd,cmdlen);
	while(SPI_read_status()!=PN532_SPI_READY)
	{
		if (timeout!=0)
		{
			timer+=10;
			if(timer > timeout) return FALSE;
			usleep(10000);
		}
	}

	if(!SPI_read_ack()) return FALSE;
	timer=0;
	while(SPI_read_status()!=PN532_SPI_READY)
	{
		if (timeout!=0)
		{
			timer+=10;
			if(timer > timeout) return FALSE;
			usleep(10000);
		}
	}

	return TRUE;
}

/**
* @brief	Funci�n que configura el m�dulo PN532 para leer las tarjetas
* 			en modo de funcionamiento pasivo
* @return	[unsigned int] - Confirmaci�n de que la configuraci�n se ha
* 			hecho correctamente
*/
unsigned int PN532_sam_config()
{
    pn532_packetbuffer[0] = PN532_SAMCONFIGURATION;
    pn532_packetbuffer[1] = 0x01; // normal mode;
    pn532_packetbuffer[2] = 0x14; // timeout 50ms * 20 = 1 second
    pn532_packetbuffer[3] = 0x01; // use IRQ pin!

    if (!PN532_send_command_check_ack(pn532_packetbuffer, 4, 0)) return FALSE;
    SPI_read_data(pn532_packetbuffer, 8);

    return  (pn532_packetbuffer[5+1] == 0x15);
}

/**
* @brief	Funci�n que lee el identificador de una tarjeta pasiva
* @param	[unsigned char] cardbaudrate - Baudrate
* @return	[uint32_t] - Identificador de la tarjeta
*/
uint32_t PN532_read_passive_target_id(unsigned char cardbaudrate)
{
	int i;
	uint32_t cid = 0;
	//unsigned int sens_res;

	pn532_packetbuffer[0] = PN532_INLISTPASSIVETARGET;
	pn532_packetbuffer[1] = 1;  // max 1 cards at once (we can set this to 2 later)
	pn532_packetbuffer[2] = cardbaudrate;

	if (!PN532_send_command_check_ack(pn532_packetbuffer, 3, 0)) return FALSE;


	// read data packet
	SPI_read_data(pn532_packetbuffer, 20);

	xil_printf("\n***Found %d tags", pn532_packetbuffer[7]);
	if (pn532_packetbuffer[7] != 1) return FALSE;
	/*sens_res = pn532_packetbuffer[9];
	sens_res <<= 8;
	sens_res |= pn532_packetbuffer[10];
	xil_printf("\nSens response: 0x%02x | Sel response: 0x%02x | id: ", sens_res, pn532_packetbuffer[11]);*/
	for (i=0; i<pn532_packetbuffer[12]; i++) {
		cid <<= 8;
		cid |= pn532_packetbuffer[13+i];
		xil_printf(" 0x%02x", pn532_packetbuffer[13+i]);
	}
	return cid;
}

/**
* @brief	Funci�n que autentica un bloque de memoria
* @param	[unsigned char] cardnumber - N�mero de tarjeta (1 o 2)
* @param	[uint32_t] cid - Identificador de la tarjeta pasiva
* @param	[unsigned char] blockaddress - N�mero de bloque que se quiere
* 			autenticar (de 0 a 63)
* @param	[unsigned char] authtype - Tipo de clave que se va a usar para
* 			la autenticaci�n (KEY_A o KEY_B)
* @param	[unsigned char *] keys - Clave de acceso con el que se desea
* 			hacer la autenticaci�n del bloque
* @return	[unsigned int] - Confirmaci�n del proceso de autenticaci�n
*/
unsigned int PN532_authenticate_block(unsigned char cardnumber /*1 or 2*/,uint32_t cid /*Card NUID*/, unsigned char blockaddress /*0 to 63*/,unsigned char authtype/*Either KEY_A or KEY_B */, unsigned char * keys)
{
	pn532_packetbuffer[0] = PN532_INDATAEXCHANGE;
	pn532_packetbuffer[1] = cardnumber;
	if(authtype == KEY_A)
	{
		pn532_packetbuffer[2] = PN532_AUTH_WITH_KEYA;
	}
	else
	{
		pn532_packetbuffer[2] = PN532_AUTH_WITH_KEYB;
	}
	pn532_packetbuffer[3] = blockaddress; //This address can be 0-63 for MIFARE 1K card

	pn532_packetbuffer[4] = keys[0];
	pn532_packetbuffer[5] = keys[1];
	pn532_packetbuffer[6] = keys[2];
	pn532_packetbuffer[7] = keys[3];
	pn532_packetbuffer[8] = keys[4];
	pn532_packetbuffer[9] = keys[5];

	pn532_packetbuffer[10] = ((cid >> 24) & 0xFF);
	pn532_packetbuffer[11] = ((cid >> 16) & 0xFF);
	pn532_packetbuffer[12] = ((cid >> 8) & 0xFF);
	pn532_packetbuffer[13] = ((cid >> 0) & 0xFF);

	if (!PN532_send_command_check_ack(pn532_packetbuffer, 14,0)) return FALSE;

	SPI_read_data(pn532_packetbuffer, 2+6);
	if((pn532_packetbuffer[6] == 0x41) && (pn532_packetbuffer[7] == 0x00))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/**
* @brief	Funci�n que lee un bloque (16 bytes) de memoria
* @param	[unsigned char] cardnumber - N�mero de tarjeta (1 o 2)
* @param	[unsigned char] blockaddress - N�mero de bloque que se quiere leer (de 0 a 63)
* @param	[unsigned char *] block - Direcci�n de memoria donde se van a guardar los
* 			datos que se almacenan en el bloque especificado
* @return	[unsigned int] - Confirmaci�n del proceso de lectura
*/
unsigned int PN532_read_memory_block(unsigned char cardnumber /*1 or 2*/,unsigned char blockaddress /*0 to 63*/, unsigned char * block)
{
	int i;

	pn532_packetbuffer[0] = PN532_INDATAEXCHANGE;
	pn532_packetbuffer[1] = cardnumber;  // either card 1 or 2 (tested for card 1)
	pn532_packetbuffer[2] = PN532_MIFARE_READ;
	pn532_packetbuffer[3] = blockaddress; //This address can be 0-63 for MIFARE 1K card

	if (!PN532_send_command_check_ack(pn532_packetbuffer, 4,0)) return FALSE;

	// read data packet
	SPI_read_data(pn532_packetbuffer, 16+2+6);

	for(i=8;i<16+2+6;i++)
	{
		block[i-8] = pn532_packetbuffer[i];

	}
	if((pn532_packetbuffer[6] == 0x41) && (pn532_packetbuffer[7] == 0x00))
	{
		return TRUE; //read successful
	}
	else
	{
		return FALSE;
	}
}

/**
* @brief	Funci�n que escribe en un bloque (16 bytes) de memoria
* @param	[unsigned char] cardnumber - N�mero de tarjeta (1 o 2)
* @param	[unsigned char] blockaddress - N�mero de bloque en el que se quiere
* 			escribir (de 0 a 63)
* @param	[unsigned char *] block - Direcci�n de memoria donde se encuentran los
* 			datos que se van a escribir en el bloque especificado
* @return	[unsigned int] - Confirmaci�n del proceso de escritura
*/
unsigned int PN532_write_memory_block(unsigned char cardnumber /*1 or 2*/,unsigned char blockaddress /*0 to 63*/, unsigned char * block)
{
	int byte;

    pn532_packetbuffer[0] = PN532_INDATAEXCHANGE;
    pn532_packetbuffer[1] = cardnumber;  // either card 1 or 2 (tested for card 1)
    pn532_packetbuffer[2] = PN532_MIFARE_WRITE;
    pn532_packetbuffer[3] = blockaddress;

    for(byte=0; byte <16; byte++)
    {
        pn532_packetbuffer[4+byte] = block[byte];
    }

    if (!PN532_send_command_check_ack(pn532_packetbuffer, 20,0)) return FALSE;

    // read data packet
    SPI_read_data(pn532_packetbuffer, 2+6);

    if((pn532_packetbuffer[6] == 0x41) && (pn532_packetbuffer[7] == 0x00))
    {
    	return TRUE; //write successful
    }
    else
    {
    	return FALSE;
    }
}
