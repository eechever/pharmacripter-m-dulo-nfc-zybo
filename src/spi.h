 /**
 * @file    spi.h
 * @brief   Fichero que cabecera de las funciones SPI
 * @author  Eneko Echeverria
 * @date    09/04/2016
 * @version	1.0
 */

#ifndef SPI_H_
#define SPI_H_

#define CLK			10	//JF2
#define SEL			11	//JF3
#define MISO		9	//JF8
#define MOSI		13	//JF1

#define HIGH		1
#define LOW			0

/*high*/
int SPI_read_ack();

/*medium*/
unsigned char SPI_read_status();
void SPI_read_data(unsigned char *buff, int n);
void SPI_write_command(unsigned char *cmd, unsigned char cmdlen);

/*low*/
void SPI_write(unsigned char c);
unsigned char SPI_read(void);


#endif /* SPI_H_ */
