 /**
 * @file    spi.c
 * @brief   Fichero que implementa las funciones relacionadas con
 * 			el protocolo de comunicaciones SPI
 * @author  Eneko Echeverria
 * @date    09/04/2016
 * @version	1.0
 */

#include <stdio.h>
#include "sleep.h"
#include "xil_printf.h"
#include "PN532.h"
#include "mio.h"
#include "spi.h"

#define _BV(bit)	(1 << (bit))

unsigned char pn532ack[] = {0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00};

/************** high level SPI **************/

/**
* @brief	Funci�n que lee el ACK del esclavo
* @return	[int] - Confirmaci�n de la recepci�n del ACK
*/
int SPI_read_ack() {
    unsigned char ackbuff[6];
    SPI_read_data(ackbuff, 6);
    return (0 == strncmp((char *)ackbuff, (char *)pn532ack, 6));
}


/************** mid level SPI **************/

/**
* @brief	Funci�n que lee el estado del esclavo
* @return	[unsigned char] - Byte de estado
*/
unsigned char SPI_read_status()
{
	unsigned char x;

	MIO_write(SEL,LOW);
	usleep(2000);
	SPI_write(PN532_SPI_STATREAD);
	//xil_printf("\n0x%02x", PN532_SPI_STATREAD);
	x=SPI_read();
	MIO_write(SEL,HIGH);

	return x;
}

/**
* @brief	Funci�n que lee informaci�n del bus de comunicaciones SPI
* @param	[unsigned char *] buff - Direcci�n de memoria donde se van
* 			a guardar los datos le�dos
* @param	[int] n - N�mero de bytes a leer
* @return	[void]
*/
void SPI_read_data(unsigned char *buff, int n)
{
	int i;

	MIO_write(SEL,LOW);
	usleep(2000);
	SPI_write(PN532_SPI_DATAREAD);
	xil_printf("\n0x%02x - READING: ", PN532_SPI_DATAREAD);

	for (i=0 ; i<n ; i++)
	{
		usleep(1);
		buff[i]=SPI_read();
		xil_printf(" 0x%02x",buff[i]);
	}

	MIO_write(SEL,HIGH);
}

/**
* @brief	Funci�n que escribe informaci�n en bus de comunicaciones SPI
* @param	[unsigned char *] cmd - Direcci�n de memoria donde se encuentran
* 			los datos que hay que mandar
* @param	[unsigned char] cmdlen - Longitud total de los datos a enviar
* @return	[void]
*/
void SPI_write_command(unsigned char *cmd, unsigned char cmdlen)
{
	unsigned int checksum, checksum_1, i;
	unsigned char cmdlen_1;

	cmdlen++;

	xil_printf("\nSending: ");
	MIO_write(SEL,LOW);
	usleep(2000);
	SPI_write(PN532_SPI_DATAWRITE);

    checksum = PN532_PREAMBLE + PN532_PREAMBLE + PN532_STARTCODE2;
    SPI_write(PN532_PREAMBLE);
    SPI_write(PN532_PREAMBLE);
    SPI_write(PN532_STARTCODE2);

    SPI_write(cmdlen);
    cmdlen_1 =~cmdlen + 1;
    SPI_write(cmdlen_1);

    SPI_write(PN532_HOSTTOPN532);
    checksum+=PN532_HOSTTOPN532;

    xil_printf(" 0x%02x", PN532_SPI_DATAWRITE);
    xil_printf(" 0x%02x", PN532_PREAMBLE);
    xil_printf(" 0x%02x", PN532_PREAMBLE);
    xil_printf(" 0x%02x", PN532_STARTCODE2);
    xil_printf(" 0x%02x", cmdlen);
    xil_printf(" 0x%02x", cmdlen_1);
    xil_printf(" 0x%02x", PN532_HOSTTOPN532);

    xil_printf(" + Comando: ");
    for(i=0 ; i<cmdlen-1 ; i++)
    {
    	SPI_write(cmd[i]);
    	checksum+=cmd[i];
    	xil_printf(" 0x%02x", cmd[i]);
    }

    checksum_1=~checksum;
    cmdlen_1=(unsigned char) checksum_1;
    SPI_write((unsigned char) checksum_1);
    SPI_write(PN532_POSTAMBLE);
    MIO_write(SEL,HIGH);

    xil_printf(" > Checksum: ");
    xil_printf(" 0x%02x", checksum_1);
    xil_printf(" 0x%02x", PN532_POSTAMBLE);

}

/************** low level SPI **************/

/**
* @brief	Funci�n que escribe un byte en el bus de comunicaciones SPI. El
* 			modo de funcionamiento del bus es: POL=0, PHA=0
* @param	[unsigned char] c - Byte que se quiere mandar
* @return	[void]
*/
void SPI_write(unsigned char c) {
    int i;
    MIO_write(CLK,HIGH);

    for (i=0; i<8; i++) {
    	MIO_write(CLK,LOW);
        if (c & _BV(i)) {
        	MIO_write(MOSI, HIGH);
        } else {
        	MIO_write(MOSI, LOW);
        }
        MIO_write(CLK,HIGH);
    }
}

/**
* @brief	Funci�n que lee un byte del bus de comunicaciones SPI. El
* 			modo de funcionamiento del bus es: POL=0, PHA=0
* @return	[unsigned char] - Byte leido
*/
unsigned char SPI_read(void) {
    int i;
    unsigned char x = 0;

    MIO_write(CLK, HIGH);

    for (i=0; i<8; i++) {
        if (MIO_read(MISO)) {
            x |= _BV(i);
        }
        MIO_write(CLK, LOW);
        MIO_write(CLK, HIGH);
    }
    return x;
}
