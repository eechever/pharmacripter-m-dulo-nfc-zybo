################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/PN532.c \
../src/main.c \
../src/mio.c \
../src/nfc.c \
../src/spi.c 

LD_SRCS += \
../src/lscript.ld 

OBJS += \
./src/PN532.o \
./src/main.o \
./src/mio.o \
./src/nfc.o \
./src/spi.o 

C_DEPS += \
./src/PN532.d \
./src/main.d \
./src/mio.d \
./src/nfc.d \
./src/spi.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -I../../standalone_bsp_complete/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


